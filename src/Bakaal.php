<?php

namespace Finpaa\Sweden;

use Finpaa\Finpaa;

class Bakaal
{
    private static $products;
    private static $ACCESS_TOKEN;

    private static function selfConstruct()
    {
        $finpaa = new Finpaa();
        self::$products = $finpaa->Sweden()->{env('FINPAA_ENVIRONMENT')}()->Bakaal()->Products()->default();
    }

    private static function getSequenceCode($name = 'products')
    {
        if(self::$$name) {
            return self::$$name;
        }
        else {
            self::selfConstruct();
            return self::getSequenceCode($name);
        }
    }

    private static function executeMethod($methodCode, $alterations, $returnPayload = false)
    {
        $response = Finpaa::executeTheMethod($methodCode, $alterations, $returnPayload);
        return array('error' => false, 'response' => json_decode(json_encode($response), true));
    }

    static function getAccessToken()
    {
        if(self::$ACCESS_TOKEN)
        {
            return self::$ACCESS_TOKEN;
        }
        else
        {
            $alterations[]['headers']['Authorization'] = "Basic " . base64_encode(env("ROARING_CLIENT_ID") . ":" . env("ROARING_CLIENT_SECRET"));
            $response = self::login();

            if(!$response['error'] && isset($response['response']['token']))
                self::$ACCESS_TOKEN = $response['response']['token'];
            else self::$ACCESS_TOKEN = "Unauthorized";
            return self::getAccessToken();
        }
    }

    static function login()
    {
        $methodCode = self::getSequenceCode()->login();

        $alterations[]['headers'] = array(
            'Accept' => null
        );

        $alterations[]['body'] = array(
            'userId' => env('BAKAAL_USER_ID'),
            'password' => env('BAKAAL_USER_PASSWORD'),
            'partnerID' => env('BAKAAL_PARTNER_ID'),
            'ipAddress' => env('BAKAAL_IP_ADDRESS'),
        );

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function getApiFields($api)
    {
        $methodCode = self::getSequenceCode()->aPIFields();

        $alterations[]['headers'] = array(
            'Accept' => null,
            'Authorization' => self::getAccessToken()
        );

        $alterations[]['body'] = array(

            "APIName" => $api,
            'UserId' => env('BAKAAL_USER_ID'),
        );

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function serviceCatalog($serviceType, $receiverCountryCode)
    {
        $methodCode = self::getSequenceCode()->serviceCatalog();

        $alterations[]['headers'] = array(
            'Accept' => null,
            'Authorization' => self::getAccessToken()
        );

        $alterations[]['body'] = array(
            "ServiceType" => $serviceType,
            "CountryCode" => $receiverCountryCode,
            'Userid' => env('BAKAAL_USER_ID'),
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(!$response['error'] && isset($response['response']['data']))
            return $response['response'];

        return $response;
    }

    static function serviceFee($serviceType, $serviceId)
    {
        $methodCode = self::getSequenceCode()->serviceFee();

        $alterations[]['headers'] = array(
            'Accept' => null,
            'Authorization' => self::getAccessToken()
        );

        $alterations[]['body'] = array(
            "Serviceid" => $serviceId,
            "ServiceType" => $serviceType,
            'Userid' => env('BAKAAL_USER_ID'),
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(!$response['error'] && isset($response['response']['data']))
            return $response['response'];

        return $response;
    }

    static function quote($serviceId, $amount)
    {
        $methodCode = self::getSequenceCode()->quote();

        $alterations[]['headers'] = array(
            'Accept' => null,
            'Authorization' => self::getAccessToken(),
            "obey-rules" => "false",
        );

        $alterations[]['body'] = array(
            "ServiceId" => $serviceId,
            "amt" => $amount,
            'UserId' => env('BAKAAL_USER_ID'),
        );

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function validate($serviceId, $mobile)
    {
        $methodCode = self::getSequenceCode()->validate();

        $alterations[]['headers'] = array(
            'Accept' => null,
            'Authorization' => self::getAccessToken(),
            "obey-rules" => "false",
        );

        $alterations[]['body'] = array(
            "ServiceId" => $serviceId,
            "rmobileNo" => $mobile,
            'UserId' => env('BAKAAL_USER_ID'),
        );

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function remittance($body)
    {
        $methodCode = self::getSequenceCode()->remittance();

        $alterations[]['headers'] = array(
            'Accept' => null,
            'Authorization' => self::getAccessToken(),
            "obey-rules" => "false",
        );

        $body["userid"] = env('BAKAAL_USER_ID');

        $alterations[]['body'] = $body;

        $response = self::executeMethod($methodCode, $alterations);

        return $response;
    }

    static function status($transferId, $transferNumber)
    {
        $methodCode = self::getSequenceCode()->status();

        $alterations[]['headers'] = array(
            "Accept" => null,
            "Authorization" => self::getAccessToken(),
            "obey-rules" => "false",
        );

        $alterations[]['body'] = array(
            "sTransid" => $transferId,
            "sTransNo" => $transferNumber,
            "Userid" => env('BAKAAL_USER_ID'),
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(!$response['error'] && isset($response['response']['result']))
            return $response['response'];

        return $response;
    }

    static function stop($transferId, $remarks = "Pelvo system cancellation")
    {
        $methodCode = self::getSequenceCode()->stop();

        $alterations[]['headers'] = array(
            "Accept" => null,
            "Authorization" => self::getAccessToken(),
            "obey-rules" => "false",
        );

        $alterations[]['body'] = array(
            "transid" => $transferId,
            "remarks" => $remarks,
            "Userid" => env('BAKAAL_USER_ID'),
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(!$response['error'] && isset($response['response']['result']))
            return $response['response'];

        return $response;
    }

    static function cancel($transferId, $remarks = "Pelvo system cancellation")
    {
        $methodCode = self::getSequenceCode()->cancel();

        $alterations[]['headers'] = array(
            "Accept" => null,
            "Authorization" => self::getAccessToken(),
            "obey-rules" => "false",
        );

        $alterations[]['body'] = array(
            "transid" => $transferId,
            "remarks" => $remarks,
            "Userid" => env('BAKAAL_USER_ID'),
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(!$response['error'] && isset($response['response']['result']))
            return $response['response'];

        return $response;
    }

    static function getBalance()
    {
        $methodCode = self::getSequenceCode()->balance();

        $alterations[]['headers'] = array(
            "Accept" => null,
            "Authorization" => self::getAccessToken(),
            "obey-rules" => "false",
        );

        $alterations[]['body'] = array(
            "Userid" => env('BAKAAL_USER_ID'),
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(!$response['error'] && isset($response['response']['balance']))
            return $response['response'];

        return $response;
    }

    static function getPurposes()
    {
        $methodCode = self::getSequenceCode()->purposes();

        $alterations[]['headers'] = array(
            "Accept" => null,
            "Authorization" => self::getAccessToken(),
            "obey-rules" => "false",
        );

        $alterations[]['body'] = array(
            "Userid" => env('BAKAAL_USER_ID'),
        );

        $response = self::executeMethod($methodCode, $alterations);

        if(!$response['error'] && isset($response['response']['data']))
            return $response['response'];

        return $response;
    }
}
